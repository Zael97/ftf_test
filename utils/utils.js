const jwt = require('jsonwebtoken');
const maxAge = 30 * 60;
exports.createToken = username => {
    return jwt.sign({ username }, 'secret key', {
        expiresIn: maxAge
    })
}

exports.handleErrors = err => {
    //console.log('ERROR:', err.code);
    /*let errors = { dni: '', nombres: '', apellidos: '', direccion: '', celular: '', email: '', username: '', password: '', token: '', active: '' }
    if (err.message.includes('Validation error')) {
        Object.values(err.errors).forEach(({ message, path }) => {
            errors[path] = message;
        })
    }
    if (err.message === 'incorrect password') {
        errors.password = 'that password is incorrect';
    }
    if (err.message === 'incorrect username') {
        errors.username = 'that username is no registered';
    }
    if (err.message === 'token not found') {
        errors.token = 'the token was not found';
    }
    if (err.message === 'invalid token') {
        errors.token = 'the token is invalid';
    }
    if (err.message === 'inactive account') {
        errors.active = 'account is not active'
    }*/
    return err;
}

exports.verify = token => {
    try {
        const encoded = jwt.verify(token, 'secret key');
        return encoded.username;
    } catch (err) {
        throw Error('invalid token');
    }
}

exports.extractToken = req => {
    try {
        const token = req.headers['authorization'].split(' ')[1];
        return token;
    } catch (err) {
        throw Error('token not found');
    }
}

